const app = require('./choo.js');

// dev routes
// app.route('/', require('./src/pages/index.js'));
// app.route('/units', require('./src/pages/units.js'));
// app.route('/palette', require('./src/pages/palette.js'));
// app.route('/base64', require('./src/pages/base64.js'));
// app.route('/loremIpsum', require('./src/pages/loremIpsum.js'));
// app.route('/generateJson', require('./src/pages/generateJson.js'));
// app.route('/md5', require('./src/pages/md5.js'));
// app.route('/sha256', require('./src/pages/sha256.js'));
// app.route('/rgbHsl', require('./src/pages/rgbHsl.js'));

// prod routes
app.route('/dev-tools', require('./src/pages/index.js'));
app.route('/dev-tools/units', require('./src/pages/units.js'));
app.route('/dev-tools/palette', require('./src/pages/palette.js'));
app.route('/dev-tools/base64', require('./src/pages/base64.js'));
app.route('/dev-tools/loremIpsum', require('./src/pages/loremIpsum.js'));
app.route('/dev-tools/generateJson', require('./src/pages/generateJson.js'));
app.route('/dev-tools/md5', require('./src/pages/md5.js'));
app.route('/dev-tools/sha256', require('./src/pages/sha256.js'));
app.route('/dev-tools/rgbHsl', require('./src/pages/rgbHsl.js'));

// fallback
app.route('/dev-tools/404', require('./src/pages/index.js'));
app.route('/dev-tools/*', require('./src/pages/index.js'));

app.mount('body');
