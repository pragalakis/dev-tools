const app = require('../../choo.js');
const html = require('choo/html');
const css = require('sheetify');
const md5 = require('crypto-js/md5');

app.use(stateStore);

function stateStore(state, emitter) {
  state.md5 = '';
  state.text = '';

  emitter.on('text', (text) => {
    state.text = text;
    state.md5 = md5(state.text).toString();
    emitter.emit('render');
  });
}

module.exports = (state, emit) => {
  emit('DOMTitleChange', 'md5');

  const handleChange = (e) => {
    emit('text', e.target.value);
  };

  return html`
    <body class=${prefix}>
       <header>
        <h1>md5</h1>
      </header>
      <main>
        <form>
          <h2>Text</h2>
          <textarea 
            oninput=${handleChange}
            width="100%"
            height="100%"
          >${state.text}</texarea>
        </form>
        <div class="result_container">
          <h2>Result</h2>
          <textarea
            width="100%"
            height="100%"
            class="result"
          >${state.md5}</textarea>
        </div>
      </main>
    </body>
  `;
};

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-ser if;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0 auto;
    text-align: center;
  }

  :host header {
    position: absolute;
    left: 1rem;
    top: 1rem;
  }

  :host header h1 {
    margin: 0;
  }

  :host main {
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
  }

  :host textarea {
    width: 400px;
    height: 200px;
    font-size: 1.2rem;
    border: 0.1rem solid #000;
    padding: 1rem;
    resize: none;
  }

  :host h2 {
    margin: 0 0 1rem 0;
  }

  :host .result {
    width: 400px;
    height: 200px;
    overflow: auto;
    overflow-wrap: anywhere;
    background: #ffc5c5;
  }
`;
