const app = require('../../choo.js');
const html = require('choo/html');
const raw = require('choo/html/raw');
const css = require('sheetify');

app.use(stateStore);

const generateRandomString = () => {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz0123456789';
  let charNum = 3 + Math.floor(Math.random() * 17);

  let key = '';
  while (charNum > 0) {
    key += alphabet[Math.floor(Math.random() * alphabet.length)];
    charNum--;
  }
  return key;
};

const generateRandomArray = () => {
  let valuesNum = 1 + Math.floor(Math.random() * 8);

  const arr = [];
  while (valuesNum > 0) {
    const rnd = Math.random();
    if (rnd >= 0.5) {
      arr.push(
        Math.random()
          .toString(16)
          .substr(2, 1 + Math.floor(Math.random() * 8))
      );
    } else {
      arr.push(Math.random() * 10);
    }
    valuesNum--;
  }
  return arr;
};

const generateRandomJson = (n) => {
  let json = {};
  while (n > 0) {
    const key = generateRandomString();

    let value;
    let rnd = Math.random();
    if (rnd <= 0.25) {
      value = Math.random() >= 0.5 ? true : false;
    } else if (rnd <= 0.5) {
      value = Math.random() * Math.floor(1 + Math.random() * 5);
      value = Number(value.toFixed(Math.floor(1 + Math.random() * 4)));
    } else if (rnd <= 0.75) {
      value = generateRandomString();
    } else {
      value = generateRandomArray();
    }

    json[key] = value;
    n--;
  }

  return JSON.stringify(json, null, '\t');
};

function stateStore(state, emitter) {
  state.json = 1;
  state.generatedJson = '{ "key": "value" }';

  emitter.on('generate', (n) => {
    state.json = n;
    state.generatedJson = generateRandomJson(state.json);
    emitter.emit('render');
  });
}

module.exports = (state, emit) => {
  emit('DOMTitleChange', 'JSON Generator');

  const handleChange = (e) => {
    emit('generate', e.target.value);
  };

  return html`
    <body class=${prefix}>
       <header>
        <h1>JSON Generator</h1>
      </header>
      <main>
        <form>
          <input
            value=${state.json}
            oninput=${handleChange}
            min=${1}
            type="number"
          />
        </form>
        <div class="result_container">
          <h2>Result</h2>
          <div class="result"
          >
           ${state.generatedJson.split('\n').map((row, i) => {
             return html`<p class="json_line" key=${i}>
             ${row.split('\t').map((text) => {
               return html`<span>${raw('<span>&emsp;</span>')}${text}</span>`;
             })}
              </p>`;
           })}
          </div>
        </div>
      </main>
    </body>
  `;
};

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-ser if;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0 auto;
    text-align: center;
  }

  :host header {
    position: absolute;
    left: 1rem;
    top: 1rem;
  }

  :host header h1 {
    margin: 0;
  }

  :host main {
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
  }

  :host input {
    padding: 1rem;
  }

  :host .result_container h2 {
    margin: 0 0 1rem 0;
  }

  :host .result {
    width: 400px;
    height: 200px;
    font-size: 0.8rem;
    font-family: monospace;
    border: 0.1rem solid #000;
    padding: 1rem;
    overflow: auto;
    overflow-wrap: anywhere;
    background: #ffc5c5;
    text-align: left;
  }

  :host .result .json_line {
    margin: 0;
  }
`;
