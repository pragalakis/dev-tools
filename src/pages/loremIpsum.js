const app = require('../../choo.js');
const html = require('choo/html');
const css = require('sheetify');

app.use(stateStore);

const loremIpsum =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

const generateParagraphs = n => {
  let text = '';
  while (n > 0) {
    text += loremIpsum;
    text += '\n';
    n--;
  }
  return text;
};

function stateStore(state, emitter) {
  state.paragraphs = 1;
  state.generatedText = loremIpsum;

  emitter.on('generate', n => {
    state.paragraphs = n;
    state.generatedText = generateParagraphs(state.paragraphs);
    emitter.emit('render');
  });
}

module.exports = (state, emit) => {
  emit('DOMTitleChange', 'Lorem Ipsum Generator');

  const handleChange = e => {
    emit('generate', e.target.value);
  };

  return html`
    <body class=${prefix}>
       <header>
        <h1>Lorem Ipsum Generator</h1>
      </header>
      <main>
        <form>
          <input
            value=${state.paragraphs}
            oninput=${handleChange}
            min=${1}
            type="number"
          />
        </form>
        <div class="result_container">
          <h2>Result</h2>
          <div class="result"
          >
           ${state.generatedText.split('\n').map((text, i) => {
             return html`<p key=${i}>${
               text.length > 0 ? `<p>${text}</p>` : ''
             }</p>`;
           })}
          </div>
        </div>
      </main>
    </body>
  `;
};

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-ser if;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0 auto;
    text-align: center;
  }

  :host header {
    position: absolute;
    left: 1rem;
    top: 1rem;
  }

  :host header h1 {
    margin: 0;
  }

  :host main {
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
  }

  :host input {
    padding: 1rem;
  }

  :host .result_container h2 {
    margin: 0 0 1rem 0;
  }

  :host .result {
    width: 400px;
    height: 200px;
    font-size: 0.8rem;
    border: 0.1rem solid #000;
    padding: 1rem;
    overflow: auto;
    overflow-wrap: anywhere;
    background: #ffc5c5;
    text-align: left;
  }
`;
