const app = require('../../choo.js');
const html = require('choo/html');
const css = require('sheetify');

app.use(stateStore);

function stateStore(state, emitter) {
  state.base64 = '';
  state.text = '';
  state.encOrDec = 'encode';

  emitter.on('encOrDec', endOrDec => {
    state.encOrDec = endOrDec;
    state.base64 = '';
    state.text = '';
    emitter.emit('render');
  });

  emitter.on('text', text => {
    state.text = text;
    if (state.encOrDec === 'encode') {
      state.base64 = btoa(state.text);
    }
    if (state.encOrDec === 'decode') {
      state.base64 = atob(state.text);
    }
    emitter.emit('render');
  });
}

module.exports = (state, emit) => {
  emit('DOMTitleChange', 'base64');

  const handleOnEncode = e => {
    emit('encOrDec', 'encode');
  };

  const handleOnDecode = e => {
    emit('encOrDec', 'decode');
  };

  const handleChange = e => {
    emit('text', e.target.value);
  };

  return html`
    <body class=${prefix}>
       <header>
        <h1>base64</h1>
      </header>
      <main>
        <form>
          <div class="toggle ${state.encOrDec}">
            <button type="button" onclick=${handleOnEncode}>Encode</button> /
            <button type="button" onclick=${handleOnDecode}>Decode</button>
          </div>
          <textarea 
            oninput=${handleChange}
            width="100%"
            height="100%"
          >${state.text}</texarea>
        </form>
        <div class="result_container">
          <h2>Result</h2>
          <textarea
            width="100%"
            height="100%"
            class="result"
          >${state.base64}</textarea>
        </div>
      </main>
    </body>
  `;
};

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-ser if;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0 auto;
    text-align: center;
  }

  :host header {
    position: absolute;
    left: 1rem;
    top: 1rem;
  }

  :host header h1 {
    margin: 0;
  }

  :host main {
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
  }

  :host .toggle,
  :host button {
    font-size: 1.5rem;
  }

  :host button {
    padding: 0;
    background: none;
    border: none;
  }

  :host button:focus {
    outline: none;
  }

  :host textarea {
    width: 400px;
    height: 200px;
    font-size: 1.2rem;
    border: 0.1rem solid #000;
    padding: 1rem;
    resize: none;
  }

  :host .result_container h2 {
    margin: 0 0 1rem 0;
  }

  :host .result {
    width: 400px;
    height: 200px;
    overflow: auto;
    overflow-wrap: anywhere;
    background: #ffc5c5;
  }

  :host .toggle {
    margin-bottom: 1rem;
  }

  :host .toggle button {
    cursor: pointer;
  }

  :host .encode button:first-child,
  :host .decode button:nth-child(2) {
    text-decoration: underline;
  }
`;
