const app = require('../../choo.js');
const html = require('choo/html');
const css = require('sheetify');
const convert = require('color-convert');

app.use(stateStore);

function stateStore(state, emitter) {
  state.rgb = [0, 0, 0];
  state.hsl = [0, 0, 0];
  state.rgbClass = '';
  state.hslClass = '';

  emitter.on('rgb', (name, value) => {
    value = Number(value) < 0 ? 0 : Number(value);
    value = Number(value) > 255 ? 255 : Number(value);

    if (name === 'r') {
      state.rgb[0] = value;
      state.hsl = convert.rgb.hsl(value, state.rgb[1], state.rgb[2]);
    } else if (name === 'g') {
      state.rgb[1] = value;
      state.hsl = convert.rgb.hsl(state.rgb[0], value, state.rgb[2]);
    } else if (name === 'b') {
      state.rgb[2] = value;
      state.hsl = convert.rgb.hsl(state.rgb[0], state.rgb[1], value);
    }

    state.hslClass = 'active';
    state.rgbClass = '';
    emitter.emit('render');
  });

  emitter.on('hsl', (name, value) => {
    value = Number(value) < 0 ? 0 : Number(value);

    if (name === 'h') {
      value = value > 360 ? 360 : value;
      state.hsl[0] = value;
      state.rgb = convert.hsl.rgb(value, state.hsl[1], state.hsl[2]);
    } else if (name === 's') {
      value = value > 100 ? 100 : value;
      state.hsl[1] = value;
      state.rgb = convert.hsl.rgb(state.hsl[0], value, state.hsl[2]);
    } else if (name === 'l') {
      value = value > 100 ? 100 : value;
      state.hsl[2] = value;
      state.rgb = convert.hsl.rgb(state.hsl[0], state.hsl[1], value);
    }

    state.rgbClass = 'active';
    state.hslClass = '';
    emitter.emit('render');
  });
}

module.exports = (state, emit) => {
  emit('DOMTitleChange', 'RGB - HSL');

  const onRgbInput = (e) => {
    emit('rgb', e.target.name, e.target.value);
  };

  const onHslInput = (e) => {
    emit('hsl', e.target.name, e.target.value);
  };

  return html`
    <body class=${prefix}>
      <header>
        <h1>RGB - HSL</h1>
      </header>
      <main>
        <form>
          <div class=${state.rgbClass}>
            <div>
              <label>Red</label>
              <input
                oninput=${onRgbInput}
                value=${state.rgb[0]}
                name="r"
                autocomplete="off"
              />
            </div>
            <div>
              <label>Green</label>
              <input
                oninput=${onRgbInput}
                value=${state.rgb[1]}
                name="g"
                autocomplete="off"
              />
            </div>
            <div>
              <label>Blue</label>
              <input
                oninput=${onRgbInput}
                value=${state.rgb[2]}
                name="b"
                autocomplete="off"
              />
            </div>
          </div>
          <div class="color" style=${`background:
      rgb(${state.rgb[0]}, ${state.rgb[1]}, ${state.rgb[2]})
`}></div>
          <div class=${state.hslClass}> 
            <div>
              <label>Hue</label>
              <input
                oninput=${onHslInput}
                value=${state.hsl[0]}
                name="h"
                autocomplete="off"
              />
            </div>
            <div>
            <label>Saturation</label>
              <input
                oninput=${onHslInput}
                value=${state.hsl[1]}
                name="s"
                autocomplete="off"
              />
            </div>
            <div>
              <label>Luminosity</label>
              <input
                oninput=${onHslInput}
                value=${state.hsl[2]}
                name="l"
                autocomplete="off"
              />
            </div>
          </div>
        </form>
      </main>
    </body>
  `;
};

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-ser if;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0;
    height: 100vh;
  }

  :host header {
    position: absolute;
    left: 1rem;
    top: 1rem;
  }

  :host header h1 {
    margin: 0;
  }

  :host main {
    height: 100%;
    margin: 0 1rem;
  }

  :host form {
    height: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-evenly;
  }

  :host input {
    box-sizing: border-box;
    text-align: center;
  }

  :host label {
    font-weight: 600;
  }

  :host form > div {
    display: flex;
    flex-direction: column;
  }

  :host form > div > div {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
  }

  :host form > div div input {
    width: 3rem;
    margin: 0 0.25rem;
    padding: 0.25rem;
    font-size: 1rem;
    border: 1px solid black;
  }

  :host form > div div input:focus {
    outline: 0.1rem solid #000000;
  }

  :host form .active input {
    color: #f77;
  }

  :host form > label input {
    width: 2rem;
    border: none;
    padding: 0;
  }

  :host .color {
    width: 10rem;
    height: 10rem;
    display: block;
    border: 1px solid black;
  }
`;
