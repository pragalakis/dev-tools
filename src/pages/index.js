const html = require('choo/html');
const css = require('sheetify');

module.exports = (state, emit) => {
  emit('DOMTitleChange', 'dev tools');

  return html`
    <body class=${prefix}>
      <header>
        <h1>dev tools</h1>
      </header>
      <main>
        <a href="/dev-tools/units">Px to REM</a>
        <a href="/dev-tools/palette">Palette</a>
        <a href="/dev-tools/base64">Base64 decode/encode</a>
        <a href="/dev-tools/loremIpsum">Lorem Ipsum generator</a>
        <a href="/dev-tools/generateJson">Random JSON generator</a>
        <a href="/dev-tools/md5">md5 hash</a>
        <a href="/dev-tools/sha256">sha-256 hash</a>
        <a href="/dev-tools/rgbHsl">rgb-hsl</a>
      </main>
    </body>
  `;
};

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-ser if;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0;
    height: 100vh;
  }

  :host header {
    position: absolute;
    left: 1rem;
    top: 1rem;
  }

  :host header h1 {
    margin: 0;
  }

  :host main {
    height: 100%;
    margin: 0 1rem;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  :host main a {
    color: #000;
    font-size: 3rem;
  }
`;
