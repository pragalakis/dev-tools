const app = require('../../choo.js');
const html = require('choo/html');
const css = require('sheetify');
const palettes = require('nice-color-palettes');

app.use(stateStore);

function stateStore(state, emitter) {
  state.notification = 'hide';

  emitter.on('copied', () => {
    state.notification = '';
    emitter.emit('render');
    setTimeout(() => {
      state.notification = 'hide';
      emitter.emit('render');
    }, 1000);
  });
}

module.exports = (state, emit) => {
  emit('DOMTitleChange', 'Palette');

  const handleOnClick = e => {
    const color = e.target.id;
    if (color.length > 0) {
      console.log(color + ' copied');
      navigator.clipboard.writeText(color);
      emit('copied');
    }
  };

  const size = document.body.clientWidth / 5 - 5;
  return html`
    <body class=${prefix}>
      <main>
      <div class='${state.notification} notification'>
        Copied to Clipboard
      </div>
      ${palettes.map(palette => {
        return html`
          <div class="palette">
            ${palette.map(color => {
              return html`
                <div 
                  id=${color}
                  class="color" 
                  onclick=${handleOnClick}
                  style='
                    background: ${color};
                    width: ${size}px;
                  '
                 >
                  <span>${color}</span>
                </div>
            `;
            })}
           </div>
        `;
      })}
      </main>
    </body>
  `;
};

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-ser if;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0 auto;
    text-align: center;
  }

  :host main {
    height: 100%;
  }

  :host .palette {
    display: block;
  }

  :host .color {
    display: inline-block;
    padding: 1.3rem 0;
    text-align: center;
    font-size: 1.2rem;
    cursor: pointer;
  }

  :host .color span {
    opacity: 0;
    font-weight: 600;
    cursor: text;
  }

  :host .color:hover span {
    opacity: 1;
    background: inherit;
    -webkit-background-clip: text;
    background-clip: text;
    color: transparent;
    filter: invert(1) grayscale(1) contrast(9) drop-shadow(0.05em 0.05em orange);
  }

  :host .notification {
    display: block;
    background: #ffe5e5;
    position: absolute;
    top: 0.2rem;
    right: 0.2rem;
    text-align: center;
    padding: 0.5rem 1rem;
  }

  :host .hide {
    display: none;
  }
`;
