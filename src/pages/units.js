const app = require('../../choo.js');
const html = require('choo/html');
const css = require('sheetify');

app.use(stateStore);

function stateStore(state, emitter) {
  state.basePx = 16;
  state.px = state.basePx;
  state.rem = state.px / state.basePx;
  state.pxClass = '';
  state.remClass = '';

  emitter.on('rem', value => {
    state.rem = value;
    state.px = state.rem * state.basePx;
    state.pClass = 'active';
    state.rClass = '';
    emitter.emit('render');
  });

  emitter.on('px', value => {
    state.rem = value / state.basePx;
    state.px = value;
    state.rClass = 'active';
    state.pClass = '';
    emitter.emit('render');
  });

  emitter.on('basePx', basePx => {
    state.basePx = basePx;
    emitter.emit('render');
  });
}

module.exports = (state, emit) => {
  emit('DOMTitleChange', 'Units');

  const onRemInput = e => {
    emit('rem', e.target.value);
  };

  const onPxInput = e => {
    emit('px', e.target.value);
  };

  const onBaseInput = e => {
    emit('basePx', e.target.value);
  };

  return html`
    <body class=${prefix}>
      <header>
        <h1>dev tools</h1>
      </header>
      <main>
        <form>
          <h2>PX to REM converter</h1>
          <div class=${state.pClass}>
            <label>PX</label>
            <input
              oninput=${onPxInput}
              value=${state.px}
              name="px"
              autocomplete="off"
            />
          </div>
          <div class=${state.rClass}> 
            <label>REM</label>
            <input
              oninput=${onRemInput}
              value=${state.rem}
              name="rem"
              autocomplete="off"
            />
          </div>
          <label>
            Base font size: 
            <input
              oninput=${onBaseInput}
              value=${state.basePx}
              name="base"
              autocomplete="off"
            />
            px.
          </label>
        </form>
      </main>
    </body>
  `;
};

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-ser if;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0;
    height: 100vh;
  }

  :host header {
    position: absolute;
    left: 1rem;
    top: 1rem;
  }

  :host header h1 {
    margin: 0;
  }

  :host main {
    height: 100%;
    margin: 0 1rem;
  }

  :host form {
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
  }

  :host input {
    box-sizing: border-box;
    text-align: center;
  }

  :host label {
    font-weight: 600;
  }

  :host form > div input {
    display: block;
    padding: 0.25rem 1.25rem;
    font-size: 2.4rem;
    border: 1px solid black;
    width: 100%;
  }

  :host form > div input:focus {
    outline: 0.1rem solid #000000;
  }

  :host form .active input {
    color: #f77;
  }

  :host form > label input {
    width: 2rem;
    border: none;
    padding: 0;
  }
`;
